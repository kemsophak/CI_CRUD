<!-- add insert list -->
<script type="text/javascript">
	$(document).ready(function(){
    //datatables
    $('#example').DataTable();
		$('#btnadd').click(function(){
			var row=$('#main').html();
			$('#sub').append(row);
		});
	});
  //get update
  function doget(id){
    //alert(id);
    $.ajax({
             url:'<?php echo base_url(); ?>home/get_by_query',
             type: 'POST',
             data: {id:id},
             success: function(data){
              if (typeof data == 'string')
                  {
                     data = jQuery.parseJSON(data);
                  }
              $('#eid').val(data.id);
              $('#ename').val(data.name);
              $('#econtact').val(data.contact);
              $('#eaddress').val(data.address);
              $('#esex option[value='+data.sex+']').prop('selected', true);
              $('#estatus option[value='+data.status+']').prop('selected', true);
                  
             },
             error: function(){
                 alert("Fail");
             }
          });
  
  }
  //delete 
  function dodelete(id){
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(function () {
      $.ajax({
                 url:'<?php echo base_url(); ?>home/delete',
                 type: 'POST',
                 data: {id:id},
                 success: function(data){
                  $('#tbl').html(data);
                swal(
                      'Deleted!',
                      'Your file has been deleted.',
                      'success'
                    )
                 },
                 error: function(){
                     alert("Fail");
                 }
              });
      
      }, function (dismiss) {

        if (dismiss === 'cancel') {
          swal(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
        }
</script>

<!-- insert popup -->
<script>
$(document).ready(function () {
    $('#fadeandscale').popup({
        pagecontainer: '.container',
        transition: 'all 0.3s'
    });

});

// delete row insert
$(document).on("click",".del", function (e) {
  e.preventdefault;
  var id =$('.del').index(this);
  if(id==0){
   alert('You Can Not Delete this!!!');
  }else{
    $(this).parents('.row').remove();
  }
});

// do Update
$('#btn_update').on('click',function(){
          $.ajax({
             url:'<?php echo base_url(); ?>home/update',
             type: 'POST',
             data: $("#form_update").serialize(),
             success: function(data){
              swal(
                    'Good job!',
                    'Updated Successfull !!!',
                    'success'
                  )
              $('#tbl').html(data);
             },
             error: function(){
                 alert("Fail");
             }
           });
      });
</script>

<!-- Update PopUp -->
<script>
$(document).ready(function () {
    $('#slide').popup({
        focusdelay: 400,
        outline: true,
        vertical: 'top'
    });
});
</script>

<!-- style insert popup -->
<style>
#btn_bg{
  width: 150px;
  margin: 10 auto;
}
#fadeandscale {
    -webkit-transform: scale(0.8);
       -moz-transform: scale(0.8);
        -ms-transform: scale(0.8);
            transform: scale(0.8);
}
.popup_visible #fadeandscale {
    -webkit-transform: scale(1);
       -moz-transform: scale(1);
        -ms-transform: scale(1);
            transform: scale(1);
}
#slide_background {
    -webkit-transition: all 0.3s 0.3s;
       -moz-transition: all 0.3s 0.3s;
            transition: all 0.3s 0.3s;
}
#slide,
#slide_wrapper {
    -webkit-transition: all 0.4s;
       -moz-transition: all 0.4s;
            transition: all 0.4s;
}
#slide {
    -webkit-transform: translateX(0) translateY(-40%);
       -moz-transform: translateX(0) translateY(-40%);
        -ms-transform: translateX(0) translateY(-40%);
            transform: translateX(0) translateY(-40%);
}
.popup_visible #slide {
    -webkit-transform: translateX(0) translateY(0);
       -moz-transform: translateX(0) translateY(0);
        -ms-transform: translateX(0) translateY(0);
            transform: translateX(0) translateY(0);
}
/* style popup Update */

#slide_background {
    -webkit-transition: all 0.3s 0.3s;
       -moz-transition: all 0.3s 0.3s;
            transition: all 0.3s 0.3s;
}
#slide,
#slide_wrapper {
    -webkit-transition: all 0.4s;
       -moz-transition: all 0.4s;
            transition: all 0.4s;
}
#slide {
    -webkit-transform: translateX(0) translateY(-40%);
       -moz-transform: translateX(0) translateY(-40%);
        -ms-transform: translateX(0) translateY(-40%);
            transform: translateX(0) translateY(-40%);
}
.popup_visible #slide {
    -webkit-transform: translateX(0) translateY(0);
       -moz-transform: translateX(0) translateY(0);
        -ms-transform: translateX(0) translateY(0);
            transform: translateX(0) translateY(0);
}
</style>