<?php
 class Home_model extends CI_model{
 	public function __construct(){
 		parent:: __construct();
 	}
 	function index(){
 		$this->db->select("id,name,if(sex=1,'Male','Female') as sex,contact, address"); 
		$this->db->from('tbl_info');
		$this->db->where(array('status' => '1'));
		$query = $this->db->get();
		return $query->result();
 	}
 	function create($data){
 		$this->db->insert_batch('tbl_info', $data);
 	}
 	function get_by_query($id){
		$this->db->select('*');
		$this->db->from('tbl_info');
		$this->db->where('id',$id);
		$query=$this->db->get();
		return $query->result();
	}
 	function update($id,$para){
 		$feild=array(
 			'name'=>$para['name'],
 			'sex'=>$para['sex'],
 			'contact'=>$para['contact'],
 			'address'=>$para['address'],
 			'status'=>$para['status']
 		);
 		// insert data from db
 		$condition=array('id'=>$id);
 		$query=$this->db->get_where("tbl_info",$condition);
 		$result=$query->result_array();
 		if(!empty($result)){
 		  $this->db->where($condition);
 		  $this->db->update("tbl_info",$feild);	
 		}	
 	}
 	function delete($para){
 		$this->db->delete("tbl_info",$para['condition']);
 	}
 }