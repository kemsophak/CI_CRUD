<?php
// pass parameter my url browser
Class Home extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('home_model');
	}
	function index(){
		$this->data['list'] = $this->home_model->index(); // calling Post model method 
   $this->load->view('home', $this->data);
	}
	function insert(){
		if ($_POST){
	        $name=$this->input->post('name');
	        $sex=$this->input->post('sex');
	        $contact=$this->input->post('contact');
	        $address=$this->input->post('address');
	        $status=1;
	        $data = array();
	        for ($i = 0; $i < count($this->input->post('name')); $i++)
	        {
	            $data[$i] = array(
	                'name' => $name[$i],
	                'sex' => $sex[$i],
	                'contact' => $contact[$i],
	                'address' => $address[$i],
	                'status' => $status
	            );
	        }
	        $this->home_model->create($data);
        } 
        $this->index();
	}
	function delete(){
		$id=$this->input->post('id');	
		$data['condition']=array('id'=>$id);
		$this->home_model->delete($data);
		$this->get_tbl();
	}
	function get_by_query(){
		$id=$this->input->post('id');
		$result=$this->home_model->get_by_query($id);
		if(count($result)>0){
			foreach($result as $r){
			echo json_encode(array('id'=>$r->id,'name'=>$r->name,'sex'=>$r->sex,'contact'=>$r->contact,'address'=>$r->address,'status'=>$r->status));
		    }
		}
	}
	function update(){
		$id=$this->input->post('eid');
		$data['name']=$this->input->post('ename');
		$data['sex']=$this->input->post('esex');
		$data['contact']=$this->input->post('econtact');
		$data['address']=$this->input->post('eaddress');
		$data['status']=$this->input->post('estatus');
		$update=$this->home_model->update($id,$data);
		  $this->get_tbl();
	
	}
	function get_tbl(){
		$tables ='';
		$result=$this->home_model->index();
		if(count($result)>0){
			foreach($result as $u){
				$tables .='<tr>';
					$tables .='<td>'.$u->id.'</td>';
					   $tables .='<td>'.$u->name.'</td>';
					    $tables .='<td>'.$u->sex.'</td>';
					    $tables .='<td>'.$u->contact.'</td>';
					    $tables .='<td>'.$u->address.'</td>';
					     $tables .='<td align="center">';
					      $tables .='<a class="initialism slide_open btn btn-warning" Onclick="doget('.$u->id.')">Edit</a> &nbsp;<a class="btn btn-danger" Onclick="dodelete('.$u->id.')">Delete</a>';
					        $tables .='</td>';
					      $tables .='</tr>';
			}
			echo ($tables);
		}
	}
}