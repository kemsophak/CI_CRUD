<!DOCTYPE html>
<html>
<head>
	<title>First Codeigniter</title>
 <?php include "include.php"; ?>
</head>
<body>
<div class="row ">
<div class="col-sm-2">
	<a class="initialism fadeandscale_open btn btn-info" href="#fadeandscale">Insert Information</a>
</div>
		<div class="col-sm-8">
			<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead >
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Gander</th>
						<th>Contact</th>
						<th>Address</th>
						<th>Option</th>
					</tr>
				</thead>
				<tbody id="tbl">
					<?php
		foreach ($list as $u ) {
		        echo "<tr>"
					."<td>$u->id</td>"
					."<td>$u->name</td>"
					."<td>$u->sex</td>"
					."<td>$u->contact</td>"
					."<td>$u->address</td>"
					."<td align='center'><a class='initialism slide_open btn btn-warning' Onclick='doget($u->id)'>Edit</a> &nbsp;<a class='btn btn-danger' Onclick='dodelete($u->id)'>Delete</a>
					</td>"
				."</tr>";
		}
	?>
				</tbody>
			</table>
		</div>
</div>

<div id="fadeandscale" class="well" >

	<div >
      <div class="row">
      	<div class="col-sm-3">
      		<label >Name:</label>
      	</div>
      	<div class="col-sm-2">
      		<label >Sex:</label>
      	</div>
      	<div class="col-sm-3">
      		<label >Contact:</label>
      	</div>
      	<div class="col-sm-3">
       <label >Address :</label>
      	</div>
      	<div class="col-sm-1">
       <input type="button" name="btnadd" class="btn btn-primary" value="+" id="btnadd">
      	</div>
      </div>
    </div>
    <br/>
    <form action="<?= base_url()?>home/insert" method="post" >
        <div  id="main">
          <div class="row">
          	<div class="col-sm-3">
          		<input type="text" name="name[]" class="form-control">
          	</div>
          	<div class="col-sm-2">
          		<select class="form-control" name="sex[]">
          			<option value="1">Male</option>
          			<option value="0">Female</option>
          		</select>
          	</div>
          	<div class="col-sm-3">
          		<input type="text" name="contact[]" class="form-control">
          	</div>
          	<div class="col-sm-3">
             <textarea name="address[]" class="form-control"></textarea>
          	</div>
          	<div class="col-sm-1">
             <input type="button" name="del" value="-" class="btn btn-danger del">
          	</div>
          	<br/>
          	<br/>
          </div>
          
        </div>
        <div id="sub">
        	
        </div>
    <div id="btninsert">
      <input type="submit" name="btnsubmit" value="Save" class="btn btn-primary">
      <button class="fadeandscale_close btn btn-default">Close</button>
    </div>
  </form>
</div>

<!-- Modal Update -->
<div id="slide" class="well" style="width: 700px;">
<form action="<?= base_url()?>home/update" method="post" id="form_update">
  <h2 align="center">Form Edit</h2>
    <input type="hidden" name="eid" id="eid">
    Name: 
    <input type="text" name="ename" id="ename" class="form-control">
    Gender: 
    <select class="form-control" name="esex" id="esex">
        <option value="1">Male</option>
        <option value="0">Female</option>
    </select>
    Contact: 
    <input type="text" name="econtact" class="form-control" id="econtact">
    Address:
    <textarea name="eaddress" id="eaddress" class="form-control"></textarea>
    Status: 
    <select class="form-control" name="estatus" id="estatus">
        <option value="1">Yes</option>
        <option value="0">No</option>
    </select>
    <div id="btn_bg">
    <button class="slide_close standalone_open btn btn-primary" id="btn_update">Update</button>
    <button class="slide_close btn btn-default">Close</button>
    </div>
</form>

</div>	
</body>
</html>
  
<?php include "script.php"; ?>


