 <?php
 $config=array(
		   		array(
		   			'field'=>'u_name',
			   		'label'=>'User Name',
			   		'rules'=>'trim|required|min_length[4]|max_length[50]',
			   		'errors'=>array(
			   			'required'=>'Please Input ',
			   			'min_length'=>'At Leasr 4 Character!!!',
			   			'max_length'=>'50 Character Only !!!'
		   			),
		   		),
		   		array(
		   			'field'=>'u_email',
			   		'label'=>'Email Address',
			   		'rules'=>'trim|required|is_unique[tbluser.u_email]|valid_email',
			   		'errors'=>array(
			   			'required'=>'Please Input !!!',
			   			'is_unique'=>'Choose  Other Email !!!',
			   			'valid_email'=>'Email Is Invalid !!!'
		   			),
		   		),
		   		array(
		   			'field'=>'u_password',
			   		'label'=>'Password',
			   		'rules'=>'trim|required|min_length[6]|max_length[32]',
			   		'errors'=>array(
			   			'required'=>'Please Input !!!',
			   			'min_length'=>'At Least 6 Character !!!',
			   			'max_length'=>'Use 32 Charactor Only !!!'
		   			),
		   		),
		   		
		   	);