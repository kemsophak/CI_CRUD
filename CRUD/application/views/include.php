<link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/css/bootstrap.min.css">
<script type="text/javascript" src="<?= base_url()?>asset/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url()?>asset/js/jquery.min.js"></script>

<!-- plugin popup -->
<script type="text/javascript" src="<?= base_url()?>asset/js/jquery.popupoverlay.js"></script>

<!-- plugin datatable -->
<script type="text/javascript" src="<?= base_url()?>asset/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= base_url()?>asset/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/css/dataTables.bootstrap4.min.css">

<!-- plugin sweet alert -->
<link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/css/sweetalert2.min.css">
 <script type="text/javascript" src="<?= base_url()?>asset/js/sweetalert2.min.js"></script>

 
